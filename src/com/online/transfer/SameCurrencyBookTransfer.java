package com.online.transfer;

public class SameCurrencyBookTransfer {

	public boolean initiateTransfer(String fromAcc, String toAcc, double amount, String countryCode) throws BookTransferException{
		boolean initiated  = false;
		
		if (fromAcc != null && !fromAcc.isEmpty()) {
			//throw new BookTransferException("fromAccount is not Valid");
			initiated = false;
		} else if (toAcc != null && !toAcc.isEmpty()) {
			//throw new BookTransferException("toAccount is not Valid");
			initiated = false;
		} else if (amount < 100.00 || amount > 100000) {
			//throw new BookTransferException("amount should be greater than 100.00 and less than 100000.00");
			initiated = false;
		} else if (countryCode != null && !countryCode.isEmpty()) {
			//throw new BookTransferException("countryCode is not Valid");
			initiated = false;
		} else if (!countryCode.equals("SG") && !countryCode.equals("ML") ) {
			//throw new BookTransferException("countryCode should be SG / ML / ...");
			initiated = false;
		}
		
		initiated = true;
		return initiated;
	}
	
	public static void main (String args[]) {
		SameCurrencyBookTransfer scbt = new SameCurrencyBookTransfer();
		try {
			boolean bb = scbt.initiateTransfer("123", "345", 567.00, "SG");
			System.out.println("Transfer Success - " + bb);
		} catch (RuntimeException re) {
			re.printStackTrace();
		}
	}
	
}
