Feature: Book Transfer from an Account to Another

  Scenario: Successfull Book Transfer
    Given fromAccount and toAccount are valid
    And amount is valid and below 100000.00
    And countryCode falls in SG, ML
    When online user initiate Book transfer
    Then amount should be transfered and event will be looged in log file